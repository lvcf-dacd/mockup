#### Design Mockup

###### What we use

Chúng tôi dựa trên nền tảng [Ant Design](ant.design) để thiết kế nên mockup này. Tất cả bản quyền của các component thuộc về Ant Design.

###### What we have

Sau khi thiết kế, chúng tôi đã có một tập các file .html mẫu cho toàn bộ dự án, cùng với các hình .png capture lại từ những file .html đó. Việc phát triển website sau này sẽ dựa trên design này

###### Next step

Phát triển website
